package main

import (
  "fmt"
  "math"
)

func main() {
  for A := uint8(0); A != math.MaxUint8-1; A++ { // Assign A range of 0..254
    for G := A+1; G != 0; G++ { // Assign G range of 1..255, 255 + 1 == 0
      var pool = []uint8{}
      var lookup = map[rune]uint8{}

      defineA(A, &pool, lookup)
      defineG(G, &pool, lookup)
      if !isPoolUnique(&pool) {
        fmt.Printf("Pool was not unique on defining A and G; %v\n", &pool)
        continue
        } else {
          fmt.Println("Upon defining A and G the pool was unique.")
        }
        defineT(&pool, lookup)
        defineC(&pool, lookup)
        if !isPoolUnique(&pool) {
          fmt.Printf("Pool was not unique on defining T and C: %v\n", &pool)
          continue
          } else {
            fmt.Println("Upon defining T and C the pool was unique.")
          }
          defineDouble('R', 'A', 'G', &pool, lookup)
          defineDouble('Y', 'T', 'C', &pool, lookup)
          defineDouble('S', 'G', 'C', &pool, lookup)
          defineDouble('W', 'A', 'T', &pool, lookup)
          defineDouble('K', 'T', 'G', &pool, lookup)
          defineDouble('M', 'A', 'C', &pool, lookup)
          if !isPoolUnique(&pool) {
            fmt.Printf("Pool was not unique upon defining doubles %v\n", &pool)
            continue
            } else {
              fmt.Println("Pool was still unique with double combinations.")
            }
            fmt.Println("Found a unique set of values.")
            for key, val := range lookup {
              fmt.Printf("Key: %[1]s, Binary Val: %[2]b, Decimal Val: %[2]d\n",
                string(key),
                val)
            }
            break
          }
        }
        fmt.Println("Searched every combination of values 0-255.")
      }
/*
Define the initial four nucleotides
*/
func defineA(val uint8, pool *[]uint8, lookup map[rune]uint8) uint8 {
  A := val
  *pool = append(*pool, A)
  lookup['A'] = A
  return A
}

func defineG(val uint8, pool *[]uint8, lookup map[rune]uint8) uint8 {
  G := val
  *pool = append(*pool, G)
  lookup['G'] = G
  return G
}

func defineT(pool *[]uint8, lookup map[rune]uint8) {
  T := ^lookup['A']
  *pool = append(*pool, T)
  lookup['T'] = T
}

func defineC(pool *[]uint8, lookup map[rune]uint8) {
  C := ^lookup['G']
  *pool = append(*pool, C)
  lookup['C'] = C
}


/*
Define the double combination codes
*/
func defineDouble(defines rune, fir rune, sec rune, pool *[]uint8, lookup map[rune]uint8) {
  nuc := (lookup[fir] | lookup[sec]) ^ (0 << lookup[fir])
  fmt.Printf("%b | %b = %b for %s\n", lookup[fir], lookup[sec], nuc, string(defines))
  *pool = append(*pool, nuc)
  lookup[defines] = nuc
}

/*
Define the triple combination codes
*/
func defineTriple(defines rune, double rune, third rune, pool *[]uint8, lookup map[rune]uint8) {
  nuc := lookup[double] & lookup[third]
  *pool = append(*pool, nuc)
  lookup[defines] = nuc
}

func isPoolUnique(pool *[]uint8) bool {
  for i := 0; i < len(*pool); i++ {
    for j := i+1; j < len(*pool); j++ {
      if (*pool)[i] == (*pool)[j] {
        return false
      }
    }
  }
  return true
}
