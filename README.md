# bit-nuc-search

A super quick and dirty search for a compressed DNA nucleotide representation in hopes to find an encoding that allows the 
basic operations of DNA sequences (complement, reversal, translation) to be implemented as bitwise operations reducing both time and space of working with the representation.

The intent was to utilize this encoding behind Go's implicit interfacing to speed up certain algorithms that disproportionately rely on operations that are efficient under this encoding.

The current conclusion (but certainly not proved final conclusion) is that there exists no encoding within byte-per-nucleotide space that satisfies all desired conditions.